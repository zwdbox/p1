# 段落：EParagraph

```json
{
  "ParagraphNum": 1,  //0表示标题段落，1表示第1自然段，依此类推
  "Text": "央视网消息：“继续、*****“改革开放排头兵”的优势。", //段落的全部纯文字
  "TextBrief": "央视网消息：...兵”的优势。", //简略文字,取段落前后各5个文字
  "ParagraphBrief": "标题:“央视网消息：...兵”的优势。”" //段落的简略文字，加上了“标题”，或者“第几段”前缀字符
  "IsEmpty":false, //段落是不是空段落 [false,true]
//段落的属性集合  
  "EParagraphProperties": {  
        "EJustification": "两端对齐",  //[左对齐, 居中, 右对齐, 两端对齐, 分散对齐]
        "OutlineLevel": "正文文本",  //大纲级别 [正文文本，1级，2级，3级，4级，5级，6级，7级，8级，9级]
  //段落的段落缩进
        "EParagraphPropertiesIndentation": { 
            "Left": { //左侧
                "Val": 2.5,
                "Unit": "字符"
            },
            "Right": { //右侧
                "Val": 3.5,
                "Unit": "字符"
            },
            "SpecialFormat": "悬挂缩进",  //特殊格式  [null，首行缩进,悬挂缩进]
            "Val": { //缩进值
                "Val": 0.9,
                "Unit": "厘米"
            }
        },
 //段落的间距            
        "EParagraphPropertiesSpacing": {  
            "Before": {  //间距_段前
                "Val": 18.0,
                "Unit": "磅"
            },
            "After": { //间距_段后
                "Val": 18.0,
                "Unit": "磅"
            },
            "Line": "固定值",  //行距
            "Val": "16磅" //设置值
        },
        "ProjectSymbol":  "实心菱形符号", //[项目符号 [圆形符号,方形符号,实心菱形符号,勾形符号,右向箭头,空心菱形符号]
        "ProjectNumbering": "一、",  //编号
        "Heading": "标题 2", //标题
  //段落的首字下沉            
        "EParagraphFirstCharDrop": { 
            "Position": "下沉", //[下沉,悬挂]
            "Lines": { //行数
                "Val": 4,
                "Unit": "行"
            },
            "Space": {  //距正文
                "Val": 0.2,
                "Unit": "厘米"
            }
        },
  //段落的边框与底纹属性            
        "EParagraphBorderShading": {
          "Setup": "阴影",  //设置  [方框,阴影,三维,自定义]
          "Style": "第4种", //样式 同Word中的下拉框选择项
          "Color": "红色", //颜色
          "Width": {  //宽度
            "Val": 2.25,
            "Unit": "磅"
          },
          "FillColor": "黄色",  //填充颜色
          "Shading": "浅色网格", //图案
          "ShadingColor": "蓝色"  //图案颜色
        },
  //段落的字体属性，参见：JSON_EFontProperties.md
        "EFontProperties": {
           ...
        }
  },
  
//段落的ERun对象集合
  "ERuns": [
  //文本类型对象
        {
            "ERunType": "文本", //Run对象的类型 [无, 文本, 文本框, 图片, 版式图片]
            "ERunTextType": "Text", //ERunText的类型：[Text,TextWrapping,Page,Space, FieldCharBegin, FieldCharEnd, FieldCode, FieldCharSeparate,Unknow]
            "Text": "十八大之后"
            //文本的字体属性，参见：JSON_EFontProperties.md
            "EFontProperties": {
            ...
            },
        }
  //图片类型ERun对象
        {
            "ERunType": "图片", //Run对象的类型 [无, 文本, 文本框, 图片, 版式图片]
            "SimilarHash": "1100110100001111000010011011000111110001111100111110000111011111", //图片的相似Hash码，一共64位，如果仅有另一张图片与它的差异位数<10，认为两张图片是相似的。
            "PictureName": "huenphen02.png",//图片名称
            "Width": { //宽度
              "Val": 10.19,
              "Unit": "厘米"
            },
            "Height": {  //高度
              "Val": 6.67,
              "Unit": "厘米"
            },
            "ScaleWidth": { //缩放宽度，百分比，比如120%
              "Val": 100,
              "Unit": "%"
            },
            "ScaleHeight": { //缩放高度
              "Val": 100,
              "Unit": "%"
            },
            "EWrapMode": "紧密型", //环绕方式 [ 无, 嵌入型, 四周型, 紧密型, 穿越型, 上下型, 衬于文字下方, 衬于文字上方]
            "EWrapText": "两边",  //环绕文字 [无, 两边, 只在左侧, 只在右侧, 只在最宽一侧]
      //图片的边框属性
            "EPictureBorderProperties": {
              "Style": "第5种(虚线)",  //样式
              "Color": "红色",   //颜色
              "Width": {  //宽度
                "Val": 1.25,
                "Unit": "磅"
              }
            },
      //图片的填充属性      
            "EPictureFillProperties": {
              "Color": "深蓝",  //颜色
              "Transparency": { //透明度
                "Val": 8,
                "Unit": "%"
              }
            },
      //文本的字体属性，参见：JSON_EFontProperties.md                        
            "EFontProperties": {
              ...
            }
        }
  //文本框类型ERun对象
        {
            "ERunType": "文本框", //Run对象的类型 [无, 文本, 文本框, 图片, 版式图片]
            "EWrapMode": "穿越型", //环绕方式 [ 无, 嵌入型, 四周型, 紧密型, 穿越型, 上下型, 衬于文字下方, 衬于文字上方]
            "EWrapText": "两边", //环绕文字 [无, 两边, 只在左侧, 只在右侧, 只在最宽一侧]
            "Text": "AB", //文本框中的文字
       //文本框的边框属性
            "EPictureBorderProperties": {
              "Style": "第2种(虚线)", //样式
              "Color": "蓝色",//颜色
              "Width": { //宽度
                "Val": 2.75,
                "Unit": "磅"
              }
            },
       //文本框的填充属性
            "EPictureFillProperties": {
              "Color": "橙色", //颜色
              "Transparency": { //透明度
                "Val": 11,
                "Unit": "%"
              }
            },
         //文本的字体属性，参见：JSON_EFontProperties.md 
            "EFontProperties": {
            
            }        
        },       
  ],
},
```