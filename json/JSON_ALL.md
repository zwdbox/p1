# JSON导出格式设计

## 全部大类：
    EWordDocumentObjects //对象集
        EParagraph //段落对象集
            ERunText //文本对象集
            ERunDrawing  //图片对象集
            ERunAlternateContent //文本框对象集
        ETable //表格集
    EPageSetup //页面设置
    EColumnSettingShow //分栏设置
    EPageHeader //页眉
    EPageFooter //页脚

## 1. EWordDocumentObjects

EWordDocumentObjects存储文档中的所有对象，包括EParagraph和ETable两类

- 以下文档中的字体属性,参见：[JSON_EFontProperties.md](JSON_EFontProperties.md)
- 以下文档中的枚举属性，参见：[enum_all.md](../enum_all.md)
### 1.1 EParagraph （段落）

- 段落json格式参见[JSON_EParagraph.md](JSON_EParagraph.md)


### 1.2. ETable (表)

- 表的json格式参见[JSON_Table.md](JSON_Table.md)

  
### 2. EPageSetup (页面设置属性)

```json
"EPageSetup": {
    "PaperName": "A4",  //纸张名称（纸张大小）
    "PaperWidth": {
      "Val": 29.7,
      "Unit": "厘米"
    },
    "PaperHeight": {
      "Val": 21.0,
      "Unit": "厘米"
    },
    "EPaperDirection": "横向", //纸张方向，取值范围：{横向，纵向}
    "MarginTop": {  //纸张边距，上方
      "Val": 3.18,
      "Unit": "厘米"
    },
    "MarginDown": {  //纸张边距，下方
      "Val": 3.18,
      "Unit": "厘米"
    },
    "MarginLeft": { //纸张边距，左边
      "Val": 2.54,
      "Unit": "厘米"
    },
    "MarginRight": { //纸张边距，左边
      "Val": 2.54,
      "Unit": "厘米"
    },
    "EPageSetupBorder": { //页面边框
      "ESetup": "三维",  //设置，[无, 方框, 阴影, 三维, 自定义]
      "EPageBorderStyleStyle": "艺术型",  //样式类型，[样式,艺术型]
      "Style": "第2种(蛋糕)", //样式
      "Color": "自动", //颜色
      "Width": {  //宽度
        "Val": 28.0,
        "Unit": "磅"
      }
    }
}
```   
 
### 3. EColumnSettingShow(分栏属性)
```json
  "EColumnSettingShow": {
    "Range": "整个文档",  //分栏影响的范围，["整个文档","从开始段的“段落简略文字”到结束段的“段落简略文字”"]
    "NumColumns": 3, //分栏数量 >1
    "Distance": {  //距离
      "Val": 4.5,
      "Unit": "字符"
    },
    "Seperator": true //是否显示分隔线
  }
```

## 4. 页眉属性EPageHeader
```json
"EPageHeader": {
    "HasPageNum": true, //是否有页码
    "EFontProperties": {//字体属性,参见：JSON_EFontProperties.md
        "FontName": "黑体",
        ...
    },
    "EJustification": "居中", //段落对齐方式 [左对齐，居中，右对齐，两端对齐，分散对齐]
    "Text": "第 [页码] / [总页数]页", //段落文本，其中[页码]和[总页数]表示页编号
    "EDocumentObjectType": "PageHeader" //EWordDocumentObjects的类型
}
```
  
### 5. 页脚属性EPageFooter

页脚属性EPageFooter与页眉属性EPageHeader相似：

```json
"EPageFooter": {
    "HasPageNum": true, //是否有页码
    "EFontProperties": {//字体属性
        "FontName": "黑体",
        ...
    },
    "EJustification": "居中", //[左对齐，居中，右对齐，两端对齐，分散对齐]
    "Text": "第 [页码] / [总页数]页",
    "EDocumentObjectType": "PageFooter"
}
```        
