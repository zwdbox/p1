# Table (表格)

以下JSON数据描述的图片是：

![](../img/table1.png)

```json
{
    "EDocumentObjectType": "Table"   
    "NumRows": 4, //表格的行数量
    "NumColumns": 5, //表格的列数量
    "EmptyRows": [ 3, 4 ], //空行的行号集合，从1开始，这里表示第3行和第4行是空行
    "EmptyColumns": [ 4 ], //空行的列号集合，从1开始，这里表示第4列是空列
    
//合并表格参数，下面表示表格中第2行2列到第3行3列合并了单元格，如果表格中有多个合并，只取第一个
    "TMergeTableCell": {
        "Left": 2, //合并单元格的开始列号
        "Top": 2,  //合并单元格的开始行号
        "Right": 3, //合并单元格的结束列号
        "Bottom": 3 //合并单元格的结束行号
    },
//表格边框属性：    
    "TableBorderShading": {
    "ETableBorderSetup": "全部",  //[无, 方框, 全部, 虚框网格, 自定义]
    "Style": "第1种(单实线)",
    "Color": "红色",
    "Width": {
        "Val": 0.75,
        "Unit": "磅"
    },
//表格底纹属性：    
    "ShadingFillColor": "蓝色",
    "Shading": "45%",
    "ShadingColor": "绿色"
    },
//表格的行集合    
    "ETableRows": [
     {
       "ETableCells": [ //第1行
         { "Text": "a" },//第1行第1列的单元格，单元格中的文字是a
         { "Text": "b" },//第1行第2列的单元格，单元格中的文字是b
         { "Text": "c" },
         { "Text": "" },
         { "Text": " " }
       ]
     },
     {
       "ETableCells": [
         { "Text": "" },
         { //第2行第2列的单元格，但是向右边合并一个单元格，并且向下边也合并一个单元格
           "GridSpan": 2, //向右合并1个单元格，加上单元格本身，一共是2个单元格合并。
           "Text": "",
           "VerticalMerge": "Restart" //从这个单元格开始向下合并 [{None, Restart, Continue ]
         },
         { "Text": "" },
         { "Text": "d" }
       ]
     },
     {
       "ETableCells": [
         { "Text": "" },
         {//第3行第2列的单元格，但是向右边合并一个单元格，并且连续（Continue）上边的纵向合并。
           "GridSpan": 2,//向右合并1个单元格，加上单元格本身，一共是2个单元格合并。
           "Text": "",
           "VerticalMerge": "Continue" //连续（Continue）上边的纵向合并 [{None, Restart, Continue ]
         },
         { "Text": "" },
         { "Text": "" }
       ]
     },
     {
       "ETableCells": [
         { "Text": "" },
         { "Text": "" },
         { "Text": "" },
         { "Text": "" },
         { "Text": "" }
       ]
     }
    ],
},
```        