# 字体属性：EFontProperties

```json
"EFontProperties": {
    "FontName": "黑体", //中文字体名称
    "Style": "加粗 倾斜", //取值范围：[null（表示常规）,倾斜,加粗,"加粗 倾斜"]
    "FontSize": "五号",  //[初号，小初，一号，小一，二号，小二，三号，小三，四号，小四，五号，小五，六号，小六，七号，八号]
    "Color": "深红",  //[自动,黑色,红色，***]
    "Underline": {  //字体下划线属性FontPropertiesUnderline
        "Style": "单实下划线", //参见：enum_all.md
        "Color": "红色"
    },
    "Emphasis": "着重号", //[null,着重号]
    "Strike": "单删除线", //[null,单删除线,双删除线]
    "Superscript": "上标", //[null,上标,下标]
    "Shadow": "阴影",    //[null,阴影]
    "NoFillEmpty": "空心",  //[null,空心]
    "Scale": "150%", //[200%,150%,100%,90%,80%,66%,50%,33%]
    "Space": "加宽",//[标准,加宽,紧缩]
    "SpacePound": { //自由值，可以取小数，如0.1磅,当间距为标准的时候，间距磅值无效
        "Val": 1.2,
        "Unit": "磅"
    },
    "Location": "提升",//[标准,提升,降低]
    "LocationPound": {//自由值，取0.5的倍数 ，如1.5磅,当位置为标准的时候，位置磅值无效
        "Val": 3.0,
        "Unit": "磅"
    }
}
```        